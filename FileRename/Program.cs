﻿using System;
using System.Globalization;
using System.Linq;
using CommandLine;
using System.Collections.Generic;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using System.Threading;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace FileRename
{
    class Program
    {
        public static void Main(string[] args)
        {
            // Create service collection
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            var optionsLoaded = false;

            new Parser(with => with.HelpWriter = Console.Error).ParseArguments<CLIOptions>(args)
                .WithParsed(options =>
                {
                    serviceCollection.AddSingleton<CLIOptions>(options);
                    optionsLoaded = true;
                });

            if (!optionsLoaded)
                return;

            // Create service provider
            var serviceProvider = serviceCollection.BuildServiceProvider();

            // Run app
            serviceProvider.GetService<ConsoleServiceWrapper>()
                .Process(CancellationToken.None)
                .Wait();
        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            // Add logging
            serviceCollection.AddSingleton(new LoggerFactory()
                .AddConsole()
                .AddDebug());
            serviceCollection.AddLogging();

            // Build configuration
            var configuration = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json", true)
                .Build();

            // Add access to generic IConfigurationRoot
            serviceCollection.AddSingleton(configuration);

            // Add services
            serviceCollection.AddTransient<IConsoleService, Processor>();
            serviceCollection.AddTransient<ConsoleServiceWrapper>();
        }
    }
}
