﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace FileRename
{
    public class Processor : IConsoleService
    {
        private readonly CLIOptions configuration;
        private readonly ILogger<Processor> logger;

        public Processor(CLIOptions configuration, ILogger<Processor> logger)
        {
            this.configuration = configuration;
            this.logger = logger;
        }

        public CLIOptions Configuration => this.configuration;

        public Task<int> Process(CancellationToken token)
        {
            return Task.Factory.StartNew(() =>
            {
                int masterCounter = 0;

                foreach (var directory in ResolveTargets())
                {
                    token.ThrowIfCancellationRequested();

                    int counter = 0;
                    logger.LogInformation($"Processing [{directory.Key}]...");

                    foreach(var file in directory.Value)
                    {
                        token.ThrowIfCancellationRequested();

                        masterCounter++;
                        counter++;

                        var newName = ResolveFileName(Path.GetFileName(file), counter);

                        File.Move(
                            file,
                            Path.Combine(Path.GetDirectoryName(file), newName));

                        logger.LogInformation($"{Path.GetFileName(file)} -> {newName} done.");
                    }
                }

                return masterCounter;
            }, token);
        }

        private string ResolveFileName(string fileName, int counter)
        {
            switch(this.configuration.Mode)
            {
                case CLIOptions.Modes.Format:
                    return string.Format(this.configuration.Template, counter);
                case CLIOptions.Modes.Regex:
                    return Regex.Replace(fileName, this.configuration.Filter, this.configuration.Template);
                default:
                    throw new NotSupportedException(this.configuration.Mode.ToString());
            }
        }

        private IEnumerable<KeyValuePair<string, IEnumerable<string>>> ResolveTargets()
        {
            var filter = new Regex(
                string.IsNullOrWhiteSpace(this.configuration.Filter) ?
                @"\.*" :
                this.configuration.Mode == CLIOptions.Modes.Format ?
                this.configuration.Filter.ToRegex() :
                this.configuration.Filter);

            var targets = (bool)this.configuration.Target?.Any() ?
                this.configuration.Target :
                new[] { Directory.GetCurrentDirectory() };

            return targets 
                .SelectMany(target => ResolveDirectory(target, filter));
        }

        private IEnumerable<KeyValuePair<string, IEnumerable<string>>> ResolveDirectory(string directory, Regex filter)
        {
            if (!Directory.Exists(directory))
                yield break;

            foreach (var subResult in Directory.EnumerateDirectories(directory)
                .SelectMany(subDirectory => ResolveDirectory(subDirectory, filter)))
                yield return subResult;

            var files = Directory.EnumerateFiles(directory)
                .Select(path => new FileInfo(path));

            switch (this.configuration.Sort)
            {
                case CLIOptions.SortModes.Alphabetical:
                    files = files.OrderBy(file => file.Name);
                    break;
                case CLIOptions.SortModes.Created:
                    files = files.OrderBy(file => file.CreationTimeUtc);
                    break;
                case CLIOptions.SortModes.LastModified:
                    files = files.OrderBy(file => file.LastWriteTimeUtc);
                    break;
                default:
                    throw new InvalidOperationException(this.configuration.Sort.ToString());
            };

            yield return new KeyValuePair<string, IEnumerable<string>>(
                directory,
                files
                    .Where(info => filter.IsMatch(info.FullName))
                    .Select(info => info.FullName));
        }
    }
}
