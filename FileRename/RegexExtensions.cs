﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace FileRename
{
    public static class RegexExtensions
    {
        public static string ToRegex(this string wildcardPattern)
        {
            var result = new StringBuilder();
            var parts = wildcardPattern.Split('*', '?');

            int idx = 0;
            foreach (var part in parts)
            {
                result.Append(Regex.Escape(part));

                idx += part.Length;

                if (idx < wildcardPattern.Length)
                    result.Append(wildcardPattern[idx] == '*' ? ".*" : ".");

                idx++;
            }

            return result.ToString();
        }
    }
}
