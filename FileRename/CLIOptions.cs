﻿using CommandLine;
using CommandLine.Text;
using System;
using System.Collections.Generic;
using System.Text;

namespace FileRename
{
    public class CLIOptions
    {
        private readonly Modes mode;
        private readonly SortModes sort;
        private readonly string filter;
        private readonly string template;
        private readonly IEnumerable<string> target;

        public CLIOptions(Modes mode, SortModes sort, string filter, string template, IEnumerable<string> target = null)
        {
            this.mode = mode;
            this.sort = sort;
            this.filter = filter;
            this.template = template;
            this.target = target;
        }

        [Usage(ApplicationAlias = "dotnet FileRename.dll")]
        public static IEnumerable<Example> Examples
        {
            get
            {
                yield return new Example("Format mode", new CLIOptions(Modes.Format, SortModes.Alphabetical, @"*.txt", "myfile_{0:00}.txt", new string[] { @"C:/Temp", @"./q.txt" }));
                yield return new Example("Regex mode", new CLIOptions(Modes.Regex, SortModes.LastModified, @"(.*)_(\d+).txt", "myfile_$1_$2_out.txt", new string[] { @"C:/Temp", @"./q_5.txt" }));
            }
        }

        [Option('m', "mode", Default = Modes.Format, HelpText = "[Format, Regex] Filter and template resolution strategy.")]
        public Modes Mode => this.mode;

        [Option('s', "sort", Default = SortModes.Alphabetical, HelpText = "[Alphabetical, Created, LastModified] The strategy used to sorted the input files in each specified directories before processing.")]
        public SortModes Sort => this.sort;

        [Option('f', "filter", Default = null, HelpText = "Pattern filters the input files before processing.")]
        public string Filter => this.filter;

        [Option('t', "template", HelpText = "The template used to perform the name transformation process on the input files.", Required = true)]
        public string Template => this.template;

        [Value(0, Default = null, HelpText = "Folders containing files ought to be processed. Current folder used if ommitted.")]
        public IEnumerable<string> Target => this.target;

        public enum Modes : byte
        {
            Format,
            Regex
        }

        public enum SortModes : byte
        {
            Alphabetical,
            Created,
            LastModified
        }
    }
}
