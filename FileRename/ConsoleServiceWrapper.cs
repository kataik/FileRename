﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FileRename
{
    public class ConsoleServiceWrapper
    {
        private readonly IConsoleService service;
        private readonly ILogger<ConsoleServiceWrapper> logger;

        public ConsoleServiceWrapper(IConsoleService service, ILogger<ConsoleServiceWrapper> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        public async Task Process(CancellationToken token)
        {
            var source = CancellationTokenSource.CreateLinkedTokenSource(token);

            var serviceTask = this.service.Process(source.Token);
            this.logger.LogInformation($"Process started...");

            try
            {
                this.logger.LogInformation($"{await serviceTask} items processed. Operation complete.");
            }
            catch (Exception ex)
            {
                this.logger.LogError(new EventId(), ex, "An error occured during the operation.");
            }
        }
    }
}
